package com.example.demo.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.models.Film;

public interface FilmRepository extends MongoRepository<Film, String> {

}
