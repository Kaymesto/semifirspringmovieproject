package com.example.demo.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.models.Seance;

public interface SeanceRepository extends MongoRepository<Seance, String>{

}
