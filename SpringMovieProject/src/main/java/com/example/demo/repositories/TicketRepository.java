package com.example.demo.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.models.Ticket;

public interface TicketRepository extends MongoRepository<Ticket, String> {

}
