package com.example.demo.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.models.Salle;

public interface SalleRepository extends MongoRepository<Salle, String>{

}
