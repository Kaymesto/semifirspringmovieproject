package com.example.demo.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.models.Cinema;


public interface CinemaRepository extends MongoRepository<Cinema, String>  {

}
