package com.example.demo.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Salle;
import com.example.demo.services.SalleService;

@RestController
@CrossOrigin
@RequestMapping("salles")
public class SalleController {
	
	private SalleService salleService;
	
	public SalleController(SalleService salleService) {
		this.salleService = salleService;
	}

	@GetMapping("allSalles")
	public List<Salle> allSalles(){
		return salleService.findAll();
	}
	
	@GetMapping("{id}")
	public Salle allSalles(@PathVariable String id){
		return salleService.getSalle(id);
	}
	
	@PostMapping("create")
	public Salle createSalle(@RequestBody Salle salle) {
		return salleService.create(salle);
	}
	
	@PutMapping("update")
	public Salle updateSalle(@RequestBody Salle salle) {
		return salleService.update(salle);
	}
	
	@DeleteMapping("delete/{id}")
	public void deleteSalle(@PathVariable String id) {
		salleService.delete(id);
	}


}
