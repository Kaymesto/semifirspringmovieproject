package com.example.demo.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Cinema;
import com.example.demo.services.CinemaService;

@RestController
@CrossOrigin
@RequestMapping("cinema")
public class CinemaController {
	
	private CinemaService cinemaService;
	
	public CinemaController(CinemaService cinemaService) {
		this.cinemaService = cinemaService;
	}
	
	@GetMapping("allCinemas")
	public List<Cinema> getAllCinemas() {
		return cinemaService.getAllCinema();
	}
	
	@GetMapping("cinema/{id}")
	public Cinema getCinema(@PathVariable String id) {
		return cinemaService.getCinema(id);
	}
	
	@PostMapping("createCinema")
	public Cinema createCinema(@RequestBody Cinema cinema) {
		return cinemaService.create(cinema);
	}
	
	@PutMapping("updateCinema")
	public Cinema updateCinema(@RequestBody Cinema cinema) {
		return cinemaService.update(cinema);
	}
	
	@DeleteMapping("deleteCinema/{id}")
	public void deleteCinema(@PathVariable String id) {
		cinemaService.delete(id);
	}
	

}
