package com.example.demo.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Seance;
import com.example.demo.services.SeanceService;

@RestController
@CrossOrigin
@RequestMapping("seance")
public class SeanceController {

	private SeanceService seanceService;
	
	public SeanceController(SeanceService seanceService) {
		this.seanceService = seanceService;
	}
	
	@GetMapping("seances")
	public List<Seance> allSeances(){
		return seanceService.findAll();
	}
	
	@GetMapping("{id}")
	public Seance findSeance(@PathVariable String id) {
		return seanceService.getSeance(id);
	}
	
	@PostMapping("create")
	public Seance createSeance(@RequestBody Seance seance) {
		return seanceService.create(seance);
	}
	
	@PutMapping("update")
	public Seance updateSeance(@RequestBody Seance seance) {
		return seanceService.update(seance);
	}
	
	@DeleteMapping("delete/{id}")
	public void deleteSeance(@PathVariable String id) {
		seanceService.delete(id);
	}
}
