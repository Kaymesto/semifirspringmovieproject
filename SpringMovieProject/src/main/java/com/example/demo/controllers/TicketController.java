package com.example.demo.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Ticket;
import com.example.demo.models.TicketToDisplay;
import com.example.demo.services.TicketService;

@RestController
@CrossOrigin
@RequestMapping("ticket")
public class TicketController {

	private TicketService ticketService;
	
	public TicketController(TicketService ticketService) {
		this.ticketService = ticketService;
	}
	
	@GetMapping("tickets")
	public List<Ticket> findTickets(){
		return ticketService.findAll();
	}
	
	@GetMapping("ticket/{id}")
	public TicketToDisplay findTicket(@PathVariable String id) {
		TicketToDisplay ticketToDisplay = new TicketToDisplay(ticketService.findOne(id));
		return ticketToDisplay;
	}
	
	@PostMapping("create")
	public Ticket createTicket(@RequestBody Ticket ticket) {
		return null;
	}
	
	@PutMapping("update")
	public Ticket updateTicket(@RequestBody Ticket ticket) {
		return null;
	}
	
	@DeleteMapping("delete/{id}")
	public void deleteTicket(@PathVariable String id) {
		
	}
	
	
}
