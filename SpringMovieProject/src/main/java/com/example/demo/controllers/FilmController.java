package com.example.demo.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Film;
import com.example.demo.services.FilmService;

@RestController
@CrossOrigin
@RequestMapping("film")
public class FilmController {

	private FilmService filmService;
	
	public FilmController(FilmService filmService) {
		this.filmService = filmService;
	}
	
	@GetMapping("films")
	public List<Film> allFilms(){
		return filmService.findAll();
	}
	
	@GetMapping("film/{id}")
	public Film findFilm(@PathVariable String id) {
		return filmService.findOne(id);
	}
	
	@PostMapping("create")
	public Film createFilm(@RequestBody Film film) {
		return filmService.create(film);
	}
	
	@PutMapping("update")
	public Film updateFilm(@RequestBody Film film) {
		return filmService.update(film);
	}
	
	@DeleteMapping("delete/{id}")
	public void deleteFilm(@PathVariable String id) {
		filmService.delete(id);
	}
	
	
}
