package com.example.demo.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Salle {
	
	@Id
	private String id;
	
	private Integer numeroSalle;
	
	private Integer nombrePlaces;
	
	@DBRef
	@Field("cinema")
	private Cinema cinema;
	
	@DBRef
	@Field("seances")
	private List<Seance> listeSeances = new ArrayList<Seance>();
	
	
	

}
