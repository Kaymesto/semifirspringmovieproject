package com.example.demo.models;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TicketToDisplay {
	


	private String Commande;
	
	private String cinema;
	
	private String film;
	
	private Date dateDébut;
	
	private Date dateFin;
	
	private Integer numeroSalle;
	
	public TicketToDisplay(Ticket ticket) {
		this.Commande = ticket.getId();
		this.cinema = ticket.getCinema().getNom();
		this.film = ticket.getSeance().getFilm().getNom();
		this.dateDébut = ticket.getSeance().getDate();
		Date dateTime = (Date) ((Temporal) this.dateDébut).plus(Duration.of(ticket.getSeance().getFilm().getDuree(), ChronoUnit.MINUTES));
		this.dateFin = dateTime;
		this.numeroSalle = ticket.getSeance().getSalle().getNumeroSalle();
	}
	

}
