package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.repositories.CinemaRepository;
import com.example.demo.repositories.FilmRepository;
import com.example.demo.repositories.SalleRepository;
import com.example.demo.repositories.SeanceRepository;
import com.example.demo.repositories.TicketRepository;
import com.example.demo.services.CinemaService;
import com.example.demo.services.FilmService;
import com.example.demo.services.SalleService;
import com.example.demo.services.SeanceService;
import com.example.demo.services.TicketService;
import com.example.demo.services.servicesImpl.CinemaServiceImpl;
import com.example.demo.services.servicesImpl.FilmServiceImpl;
import com.example.demo.services.servicesImpl.SalleServiceImpl;
import com.example.demo.services.servicesImpl.SeanceServiceImpl;
import com.example.demo.services.servicesImpl.TicketServiceImpl;

@Configuration
public class AppConfig {
	
	@Bean
	public CinemaService cinemaService(CinemaRepository cinemaRepository) {
		return new CinemaServiceImpl(cinemaRepository);
	}
	
	@Bean
	public SeanceService seanceService(SeanceRepository seanceRepository) {
		return new SeanceServiceImpl(seanceRepository);
	}
	
	@Bean
	public SalleService salleService(SalleRepository salleRepository) {
		return new SalleServiceImpl(salleRepository);
	}
	
	@Bean
	public FilmService filmService(FilmRepository filmRepository) {
		return new FilmServiceImpl(filmRepository);
	}
	
	@Bean
	public TicketService ticketService(TicketRepository ticketRepository) {
		return new TicketServiceImpl(ticketRepository);
	}

}
