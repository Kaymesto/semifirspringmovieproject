package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Film;

public interface FilmService {
	
	public List<Film> findAll();
	
	public Film findOne(String id);
	
	public Film create(Film film);
	
	public Film update(Film film);
	
	public void delete(String id);

}
