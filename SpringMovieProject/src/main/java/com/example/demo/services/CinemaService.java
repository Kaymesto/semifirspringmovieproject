package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Cinema;

public interface CinemaService {
	
	public List<Cinema> getAllCinema();
	
	public Cinema getCinema(String id);
	
	public Cinema create(Cinema cinema);
	
	public Cinema update(Cinema cinema);
	
	public void delete(String id);

}
