package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Ticket;
import com.example.demo.repositories.TicketRepository;
import com.example.demo.services.TicketService;

public class TicketServiceImpl implements TicketService {
	
	private TicketRepository ticketRepository;
	
	public TicketServiceImpl(TicketRepository ticketRepository) {
		this.ticketRepository = ticketRepository;
	}

	@Override
	public List<Ticket> findAll() {
		return ticketRepository.findAll();
	}

	@Override
	public Ticket findOne(String id) {
		return ticketRepository.findById(id).get();
	}

	@Override
	public Ticket createTicket(Ticket ticket) {
		return ticketRepository.save(ticket);
	}

	@Override
	public Ticket updateTicket(Ticket ticket) {
		return ticketRepository.save(ticket);

	}

	@Override
	public void deleteTicket(String id) {
		ticketRepository.deleteById(id);
	}

}
