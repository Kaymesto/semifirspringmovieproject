package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Salle;
import com.example.demo.repositories.SalleRepository;
import com.example.demo.services.SalleService;

public class SalleServiceImpl implements SalleService {
	
	private SalleRepository salleRepository;
	
	public SalleServiceImpl(SalleRepository salleRepository) {
		this.salleRepository = salleRepository;
	}

	@Override
	public List<Salle> findAll() {
		return salleRepository.findAll();
	}

	@Override
	public Salle getSalle(String id) {
		return salleRepository.findById(id).get();
	}

	@Override
	public Salle create(Salle salle) {
		return salleRepository.save(salle);
	}

	@Override
	public Salle update(Salle salle) {
		return salleRepository.save(salle);
	}

	@Override
	public void delete(String id) {
		salleRepository.deleteById(id);
	}

}
