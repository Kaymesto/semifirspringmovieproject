package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Film;
import com.example.demo.repositories.FilmRepository;
import com.example.demo.services.FilmService;

public class FilmServiceImpl implements FilmService {
	
	private FilmRepository filmRepository;
	
	public FilmServiceImpl(FilmRepository filmRepository) {
		this.filmRepository = filmRepository;
	}

	@Override
	public List<Film> findAll() {
		return filmRepository.findAll();
	}

	@Override
	public Film findOne(String id) {
		return filmRepository.findById(id).get();
	}

	@Override
	public Film create(Film film) {
		return filmRepository.save(film);
	}

	@Override
	public Film update(Film film) {
		return filmRepository.save(film);
	}

	@Override
	public void delete(String id) {
		filmRepository.deleteById(id);
	}
	
	

}
