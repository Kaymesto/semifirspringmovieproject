package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Seance;

public interface SeanceService {
	
	public List<Seance> findAll();
	
	public Seance getSeance(String id);
	
	public Seance create(Seance seance);
	
	public Seance update(Seance seance);
	
	public void delete(String id);

}
