package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Seance;
import com.example.demo.repositories.SalleRepository;
import com.example.demo.repositories.SeanceRepository;
import com.example.demo.services.SeanceService;

public class SeanceServiceImpl implements SeanceService {
	
	private SeanceRepository seanceRepository;
	
	public SeanceServiceImpl(SeanceRepository seanceRepository) {
		this.seanceRepository = seanceRepository;
	}

	@Override
	public List<Seance> findAll() {
		return seanceRepository.findAll();
	}

	@Override
	public Seance getSeance(String id) {
		return seanceRepository.findById(id).get();
	}

	@Override
	public Seance create(Seance seance) {
		return seanceRepository.save(seance);
	}

	@Override
	public Seance update(Seance seance) {
		return seanceRepository.save(seance);
	}

	@Override
	public void delete(String id) {
		seanceRepository.deleteById(id);
	}

}
