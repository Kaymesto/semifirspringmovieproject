package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Cinema;
import com.example.demo.repositories.CinemaRepository;
import com.example.demo.services.CinemaService;

public class CinemaServiceImpl implements CinemaService {
	
	private CinemaRepository cinemaRepository;
	
	public CinemaServiceImpl(CinemaRepository cinemaRepository) {
		this.cinemaRepository = cinemaRepository;
	}

	@Override
	public List<Cinema> getAllCinema() {
		return cinemaRepository.findAll();
	}

	@Override
	public Cinema getCinema(String id) {
		return cinemaRepository.findById(id).get();
	}

	@Override
	public Cinema create(Cinema cinema) {
		return cinemaRepository.save(cinema);
	}

	@Override
	public Cinema update(Cinema cinema) {
		return cinemaRepository.save(cinema);
	}

	@Override
	public void delete(String id) {
		cinemaRepository.deleteById(id);
	}
	
	

}
