package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Ticket;

public interface TicketService {
	
	public List<Ticket> findAll();
	
	public Ticket findOne(String id);
	
	public Ticket createTicket(Ticket ticket);

	public Ticket updateTicket(Ticket ticket);
	
	public void deleteTicket(String id);
	
}
