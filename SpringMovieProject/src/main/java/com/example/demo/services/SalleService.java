package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Salle;

public interface SalleService {
	
	public List<Salle> findAll();
	
	public Salle getSalle(String id);
	
	public Salle create(Salle salle);
	
	public Salle update(Salle salle);
	
	public void delete(String id);

}
